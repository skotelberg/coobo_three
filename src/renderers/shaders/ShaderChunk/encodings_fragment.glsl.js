export default /* glsl */`
#if !defined( GLOBAL_TONEMAPPING )
    gl_FragColor = linearToOutputTexel( gl_FragColor );
#endif
`;
