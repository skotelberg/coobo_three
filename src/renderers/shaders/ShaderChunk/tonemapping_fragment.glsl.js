export default /* glsl */`
#if defined( TONE_MAPPING ) && !defined( GLOBAL_TONEMAPPING )

    gl_FragColor.rgb = toneMapping( gl_FragColor.rgb );

#endif
`;
