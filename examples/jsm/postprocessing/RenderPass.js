// import {
// 	Color
// } from '../../../build/three.module.js';
import { Pass } from '../postprocessing/Pass.js';
import { Color } from '../../../src/math/Color';

var RenderPass = function ( scene, camera, overrideMaterial, clearColor, clearAlpha ) {

	Pass.call( this );

	this.scene = scene;
	this.camera = camera;

	this.overrideMaterial = overrideMaterial;

	this.clearColor = clearColor;
	this.clearAlpha = ( clearAlpha !== undefined ) ? clearAlpha : 0;

	this.clear = true;
	this.clearDepth = false;
	this.needsSwap = false;
	this._oldClearColor = new Color();

};

RenderPass.prototype = Object.assign( Object.create( Pass.prototype ), {

	constructor: RenderPass,

    render: function ({ renderer, readBuffer, maskActive }) {

		const oldAutoClear = renderer.autoClear;
        renderer.autoClear = false;

        const stencil = renderer.state.buffers.stencil;

		let oldClearAlpha, oldOverrideMaterial;

		if ( this.overrideMaterial !== undefined ) {

			oldOverrideMaterial = this.scene.overrideMaterial;

			this.scene.overrideMaterial = this.overrideMaterial;

		}

		if ( this.clearColor ) {

			renderer.getClearColor( this._oldClearColor );
			oldClearAlpha = renderer.getClearAlpha();

			renderer.setClearColor( this.clearColor, this.clearAlpha );

		}

		if ( this.clearDepth ) {

			renderer.clearDepth();

        }

        const oldEncoding = readBuffer.texture.encoding;

        if(!this.renderToScreen && "outputEncoding" in this)
            readBuffer.texture.encoding = this.outputEncoding;

		renderer.setRenderTarget( this.renderToScreen ? null : readBuffer );

		// TODO: Avoid using autoClear properties, see https://github.com/mrdoob/three.js/pull/15571#issuecomment-465669600
        if ( this.clear ) renderer.clear( renderer.autoClearColor, renderer.autoClearDepth, renderer.autoClearStencil );

        if (maskActive) {
            const oldBackground = this.scene.background;
            if (oldBackground) {
                const children = this.scene.children;

                stencil.setLocked(false);
                stencil.setTest(false);
                stencil.setLocked(true);

                this.scene.children = [];
                renderer.render(this.scene, this.camera);
                this.scene.children = children;

                stencil.setLocked(false);
                stencil.setTest(true);
                stencil.setLocked(true);
            }

            this.scene.background = null;
            renderer.render(this.scene, this.camera);
            this.scene.background = oldBackground;
        } else {
            renderer.render(this.scene, this.camera);
        }

		if ( this.clearColor ) {

			renderer.setClearColor( this._oldClearColor, oldClearAlpha );

		}

		if ( this.overrideMaterial !== undefined ) {

			this.scene.overrideMaterial = oldOverrideMaterial;

		}

		renderer.autoClear = oldAutoClear;


        if(this.renderToScreen)
            readBuffer.texture.encoding = oldEncoding;
	}

} );

export { RenderPass };
