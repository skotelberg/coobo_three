import { CopyShader } from "../shaders/CopyShader.js";
import { ShaderPass } from "../postprocessing/ShaderPass.js";
import { MaskPass } from "../postprocessing/MaskPass.js";
import { ClearMaskPass } from "../postprocessing/MaskPass.js";

import { Clock } from "../../../src/core/Clock";
import { FloatType, LinearFilter, RGBEFormat } from "../../../src/constants";
import { Mesh } from "../../../src/objects/Mesh";
import { OrthographicCamera } from "../../../src/cameras/OrthographicCamera";
import { PlaneBufferGeometry } from "../../../src/geometries/PlaneBufferGeometry";
import { Vector2 } from "../../../src/math/Vector2";
import { WebGLRenderTarget } from "../../../src/renderers/WebGLRenderTarget";
import { ShaderMaterial } from "../../../src/materials/ShaderMaterial.js";

var EffectComposer = function ( renderer, renderTarget, stencilBuffer = false ) {

	this.renderer = renderer;

    const gl = renderer.getContext();
    this._maxTextureSize = gl.getParameter(gl.MAX_TEXTURE_SIZE)

	if ( renderTarget === undefined ) {

		var parameters = {
			minFilter: LinearFilter,
			magFilter: LinearFilter,
            format: RGBEFormat,
            type: FloatType,
            stencilBuffer
            // encoding: sRGBEncoding
		};

        var size = renderer.getSize( new Vector2() );

		this._pixelRatio = renderer.getPixelRatio();
		this._width = size.width;
		this._height = size.height;

		renderTarget = new WebGLRenderTarget( this._width * this._pixelRatio, this._height * this._pixelRatio, parameters );
		renderTarget.texture.name = 'EffectComposer.rt1';

	} else {

		this._pixelRatio = 1;
		this._width = renderTarget.width;
		this._height = renderTarget.height;

	}

	this.renderTarget1 = renderTarget;
	this.renderTarget2 = renderTarget.clone();
    this.renderTarget2.texture.name = 'EffectComposer.rt2';
    this.renderTargetMask = renderTarget.clone();
	this.renderTargetMask.texture.name = 'EffectComposer.rtMask';

	this.writeBuffer = this.renderTarget1;
    this.readBuffer = this.renderTarget2;
    this.maskBuffer = this.renderTargetMask;

	this.renderToScreen = true;

	this.passes = [];

	// dependencies

	if ( CopyShader === undefined ) {

		console.error( 'THREE.EffectComposer relies on CopyShader' );

	}

	if ( ShaderPass === undefined ) {

		console.error( 'THREE.EffectComposer relies on ShaderPass' );

	}

    this.copyPass = new ShaderPass( CopyShader );
    this.copyMaskQuad = new Pass.FullScreenQuad(new ShaderMaterial(CopyShader));
    this.copyMaskQuad.material.uniforms.discardable.value = true;

	this.clock = new Clock();

};

Object.assign( EffectComposer.prototype, {

	swapBuffers: function () {

		var tmp = this.readBuffer;
		this.readBuffer = this.writeBuffer;
		this.writeBuffer = tmp;

	},

	addPass: function ( pass ) {

		this.passes.push( pass );
		pass.setSize( this._width * this._pixelRatio, this._height * this._pixelRatio );

	},

	insertPass: function ( pass, index ) {

		this.passes.splice( index, 0, pass );
		pass.setSize( this._width * this._pixelRatio, this._height * this._pixelRatio );

	},

	removePass: function ( pass ) {

		const index = this.passes.indexOf( pass );

		if ( index !== - 1 ) {

			this.passes.splice( index, 1 );

		}

	},

	isLastEnabledPass: function ( passIndex ) {

		for ( var i = passIndex + 1; i < this.passes.length; i ++ ) {

			if ( this.passes[ i ].enabled ) {

				return false;

			}

		}

		return true;

    },
    
    transferStencil: function(renderer, maskActive, fullScreenStencil = false) {
        if (!maskActive) return;

        const gl = renderer.getContext();
        const state = renderer.state;
        const stencil = state.buffers.stencil;
        const color = state.buffers.color;
        const depth = state.buffers.depth;

        
        color.setLocked(false);
        color.setMask(false);
        color.setLocked(true);
        
        depth.setLocked(false);
        depth.setMask(false);
        depth.setLocked(true);

        if (fullScreenStencil) {
            stencil.setLocked(false);
            stencil.setClear(1);
            stencil.setLocked(true);

            renderer.clearStencil();
        } else {
            stencil.setLocked(false);
            stencil.setOp(gl.KEEP, gl.KEEP, gl.REPLACE);
            stencil.setFunc(gl.ALWAYS, 1, 0xffffffff)
            stencil.setClear(0);
            stencil.setLocked(true);

            this.copyMaskQuad.material.uniforms.tDiffuse.value = this.maskBuffer.texture;
            this.copyMaskQuad.material.uniforms.discardable.value = true;
            this.copyMaskQuad.render(renderer);
        }

        color.setLocked(false);
        depth.setLocked(false);

        stencil.setLocked(false);
        stencil.setClear(0);
        stencil.setFunc(gl.EQUAL, 1, 0xffffffff); // draw if == 1
        stencil.setOp(gl.KEEP, gl.KEEP, gl.KEEP);
        stencil.setLocked(true);
    },

	render: function ( deltaTime ) {

		// deltaTime value is in seconds

		if ( deltaTime === undefined ) {

			deltaTime = this.clock.getDelta();

		}

		var currentRenderTarget = this.renderer.getRenderTarget();

		var maskActive = false;

		var pass, i, il = this.passes.length;

		for ( i = 0; i < il; i ++ ) {

			pass = this.passes[ i ];

			if ( pass.enabled === false ) continue;

			pass.renderToScreen = ( this.renderToScreen && this.isLastEnabledPass( i ) );
            pass.render({
                renderer: this.renderer,
                writeBuffer: this.writeBuffer,
                readBuffer: this.readBuffer,
                maskBuffer: this.maskBuffer,
                funTransferStencil: fullScreenStencil => this.transferStencil(this.renderer, maskActive, fullScreenStencil),
                deltaTime,
                maskActive
            });

			if ( pass.needsSwap ) {

				if ( maskActive ) {

					var context = this.renderer.getContext();
					var stencil = this.renderer.state.buffers.stencil;

					//context.stencilFunc( context.NOTEQUAL, 1, 0xffffffff );
					stencil.setFunc( context.NOTEQUAL, 1, 0xffffffff );

                    this.copyPass.render({
                        renderer: this.renderer,
                        writeBuffer: this.writeBuffer,
                        readBuffer: this.readBuffer,
                        maskBuffer: this.maskBuffer,
                        funTransferStencil: fullScreenStencil => this.transferStencil(this.renderer, maskActive, fullScreenStencil),
                        deltaTime,
                        maskActive: false
                    });

					//context.stencilFunc( context.EQUAL, 1, 0xffffffff );
					stencil.setFunc( context.EQUAL, 1, 0xffffffff );

				}

				this.swapBuffers();

			}

			if ( MaskPass !== undefined ) {

				if ( pass instanceof MaskPass ) {

					maskActive = true;

				} else if ( pass instanceof ClearMaskPass ) {

					maskActive = false;

				}

			}

		}

		this.renderer.setRenderTarget( currentRenderTarget );

	},

	reset: function ( renderTarget ) {

		if ( renderTarget === undefined ) {

			var size = this.renderer.getSize( new Vector2() );
			this._pixelRatio = this.renderer.getPixelRatio();
			this._width = size.width;
			this._height = size.height;

			renderTarget = this.renderTarget1.clone();
			renderTarget.setSize( this._width * this._pixelRatio, this._height * this._pixelRatio );

		}

		this.renderTarget1.dispose();
        this.renderTarget2.dispose();
        this.renderTargetMask.dispose();
		this.renderTarget1 = renderTarget;
		this.renderTarget2 = renderTarget.clone();

		this.writeBuffer = this.renderTarget1;
		this.readBuffer = this.renderTarget2;

	},

	setSize: function ( width, height ) {

		this._width = width;
		this._height = height;

        const aspectRatio = height / width;

        let effectiveWidth, effectiveHeight;
        if(width >= height) {
            effectiveWidth = Math.min(this._maxTextureSize, width * this._pixelRatio);
            effectiveHeight = effectiveWidth * aspectRatio
        } else {
            effectiveHeight = Math.min(this._maxTextureSize, height * this._pixelRatio);
            effectiveWidth = effectiveHeight / aspectRatio
        }

		this.renderTarget1.setSize( effectiveWidth, effectiveHeight );
        this.renderTarget2.setSize( effectiveWidth, effectiveHeight );
        this.renderTargetMask.setSize( effectiveWidth, effectiveHeight );

		for ( var i = 0; i < this.passes.length; i ++ ) {

			this.passes[ i ].setSize( effectiveWidth, effectiveHeight );

		}

	},

	setPixelRatio: function ( pixelRatio ) {

		this._pixelRatio = pixelRatio;

		this.setSize( this._width, this._height );

	}

} );


var Pass = function () {

	// if set to true, the pass is processed by the composer
	this.enabled = true;

	// if set to true, the pass indicates to swap read and write buffer after rendering
	this.needsSwap = true;

	// if set to true, the pass clears its buffer before rendering
	this.clear = false;

	// if set to true, the result of the pass is rendered to screen. This is set automatically by EffectComposer.
	this.renderToScreen = false;

};

Object.assign( Pass.prototype, {

	setSize: function ( /* width, height */ ) {},

	render: function ( /* renderer, writeBuffer, readBuffer, deltaTime, maskActive */ ) {

		console.error( 'THREE.Pass: .render() must be implemented in derived pass.' );

	}

} );

// Helper for passes that need to fill the viewport with a single quad.
Pass.FullScreenQuad = ( function () {

	var camera = new OrthographicCamera( - 1, 1, 1, - 1, 0, 1 );
	var geometry = new PlaneBufferGeometry( 2, 2 );

	var FullScreenQuad = function ( material ) {

		this._mesh = new Mesh( geometry, material );

	};

	Object.defineProperty( FullScreenQuad.prototype, 'material', {

		get: function () {

			return this._mesh.material;

		},

		set: function ( value ) {

			this._mesh.material = value;

		}

	} );

	Object.assign( FullScreenQuad.prototype, {

		dispose: function () {

			this._mesh.geometry.dispose();

		},

		render: function ( renderer ) {

			renderer.render( this._mesh, camera );

		}

	} );

	return FullScreenQuad;

} )();

export { EffectComposer, Pass };
